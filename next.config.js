/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  pwa: {
    dest: "public",
    register: true,
    disable: process.env.NODE_ENV === 'development',
    skipWaiting: true,
  },
}
const withPWA = require('next-pwa')({
  dest: 'public'
})

module.exports = withPWA(nextConfig)
